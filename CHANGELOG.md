# Change Log
All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).  
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## 0.0.1 - 2021-08-12
 - Initial development of exchange-gal-receiver
