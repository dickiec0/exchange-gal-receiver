from exchangelib import Credentials, Account, DistributionList, Configuration


def get_gal_json():
    # import requests
    # # WARNING: You should point this to your cert file for the server to actually
    # # verify the host. Skips verification if set to false
    # CERT_FILE = False
    # authed_session = requests.Session()
    # # Add your credentials
    # authed_session.auth = ('account@somedomain.net', 'password')
    # # Cert verification, will not verify on false
    # authed_session.verify = CERT_FILE
    # authed_session.headers.update({'User-Agent': 'exchangelib/3.2.0 (python-requests/2.23.0)'})
    # # Fetch the actual data
    # fetched_data = authed_session.get('https://outlook.office365.com/EWS/Exchange.asmx')
    # print(fetched_data)

    # print('get_gal_json()')
    # credentials = Credentials('account@somedomain.net', 'password')
    # config = Configuration(server='office.office365.com', credentials=credentials)
    # a = Account('account@somedomain.net', credentials=credentials, autodiscover=False, config=config)
    #
    # folder = a.root / 'AllContacts'
    # for p in folder.people():
    #     print(p)
    #
    # gal = a.contacts / 'GAL Contacts'
    # for c in gal.all():
    #     print(c)
    #
    # for item in a.inbox.all().order_by('-datetime_received')[:100]:
    #     print(item.subject, item.sender, item.datetime_received)
    #

    return {
        'gal': [
            {
                'name': 'bob',
                'email': 'bab@bob.com'
            },
            {
                'name': 'bob',
                'email': 'bab@bob.com'
            }
        ]
    }

