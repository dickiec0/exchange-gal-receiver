from setuptools import setup

REQUIREMENTS = [
    "exchangelib==4.4.0"
]
__version__ = "0.0.1"
setup(
    name="exchange-gal-retreiver",
    version=__version__,
    packages=['exchange_gal_receiver'],
    install_requires=REQUIREMENTS,
    setup_requires=["pytest-runner", "requests-mock==1.7.0"],
    tests_require=["pytest", "freezegun==0.3.12"] + REQUIREMENTS,
)
