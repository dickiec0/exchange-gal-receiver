from unittest.mock import Mock, patch

from exchange_gal_receiver.receiver import get_gal_json


def test_get_gal_json():

    gal_json = get_gal_json()

    assert gal_json == {
        'gal': [
            {
                'name': 'bob',
                'email': 'bab@bob.com'
            },
            {
                'name': 'bob',
                'email': 'bab@bob.com'
            }
        ]
    }
