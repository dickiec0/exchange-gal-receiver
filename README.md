# exchange-gal-receiver

Functions to retreive the Global Address List (GAL) from Exchange using Exchange Web Service (EWS)

# Functions

## Metrics

`get_gal_json()`

A method to retrieve the GAL via EWS as a JSON object.

**return** A JSON object containing the GAL

# Tests

`pip install . && pip install coverage; coverage run --branch --source=exchange_gal_receiver setup.py test; coverage xml -i;coverage report`